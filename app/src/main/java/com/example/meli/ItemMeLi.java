package com.example.meli;

import org.json.JSONObject;

public class ItemMeLi {
    private String id;
    private String title;
    private int price;
    private String thumbnail;
    private JSONObject vendor;
    private String condition;
    private String permalink;
    private boolean accepts_mercadopago;
    private JSONObject shipping;

    public ItemMeLi() {

    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public boolean isAccepts_mercadopago() {
        return accepts_mercadopago;
    }

    public void setAccepts_mercadopago(boolean accepts_mercadopago) {
        this.accepts_mercadopago = accepts_mercadopago;
    }

    public JSONObject getShipping() {
        return shipping;
    }

    public void setShipping(JSONObject shipping) {
        this.shipping = shipping;
    }

    public JSONObject getVendor() {
        return vendor;
    }

    public void setVendor(JSONObject vendor) {
        this.vendor = vendor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

}
