package com.example.meli;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.os.AsyncTask;

import com.mercadolibre.android.sdk.ApiResponse;
import com.mercadolibre.android.sdk.Meli;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button btnSearch, btnDataClear;
    private EditText etSearch;
    private ArrayList<ItemMeLi> itemsMeLi = new ArrayList<ItemMeLi>();
    private ListView lvItems;
    private ListViewAdapter lvAdapter;
    private JSONArray jsonItems;
    private String apiRes;


    private Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicializeSDK(this.getApplicationContext(), this);
        inicializeItms();
        inicializeOnClickListeners(this, this.getApplicationContext());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        if(!apiRes.isEmpty()) {
            savedInstanceState.putString("lastSearch", apiRes);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        apiRes = savedInstanceState.getString("lastSearch");
        lvItems.setVisibility(View.VISIBLE);
        populateListViewApiResponse(apiRes);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
            toastMake("Log OK");
            Log.d("token after login", Meli.getCurrentIdentity(this.getApplicationContext()).getAccessToken().getAccessTokenValue());
        }
    }


    private void inicializeSDK(Context ctx, Activity activity){

        Meli.setLoggingEnabled(true);

        Meli.initializeSDK(ctx);


        Meli.startLogin(activity, 999);


    }

    private void inicializeItms(){
        btnSearch = (Button) findViewById(R.id.btnSearch);
        etSearch = (EditText) findViewById(R.id.etSearch);
        lvItems = (ListView) findViewById(R.id.lv_items);
        btnDataClear = (Button) findViewById(R.id.btn_data_clear);
    }

    private void inicializeOnClickListeners(final Activity activity, final Context ctx){
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etSearch.getText().toString().isEmpty()) {
                    new GetAsycTask().execute(new Command() {
                        @Override
                        ApiResponse executeCommand() {
                            return Meli.get("/sites/MLA/search?q="+
                                    etSearch.getText().toString()
                                    +"&access_token="+Meli.getCurrentIdentity(ctx).getAccessToken().getAccessTokenValue());

                        }
                    });

                } else {
                    toastMake("Ingrese que quiere buscar.");
                }

            }
        });

        btnDataClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAppData();
            }
        });

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent detailIntent = new Intent(MainActivity.this, DetailActivity.class);
                try {
                    final JSONObject json = jsonItems.getJSONObject(i);
                    String toPassJsonString = json.toString();
                    Log.d("ITEM", "onItemClick: " + toPassJsonString);
                    detailIntent.putExtra("itemJson", toPassJsonString);
                    startActivity(detailIntent);
                }catch (JSONException e) {
                    Log.e("ERROR ITEM CLICK", "onItemClick: " + e.getMessage());
                }

            }
        });
    }

    private class GetAsycTask extends AsyncTask<Command, Void, ApiResponse> {

        @Override
        protected void onPreExecute() {
            findViewById(R.id.pg_loading).setVisibility(View.VISIBLE);
            findViewById(R.id.lv_items).setVisibility(View.VISIBLE);
            hideKeyboardFrom(getApplicationContext(), findViewById(R.id.pg_loading));
            lvItems.setAdapter(null);
            itemsMeLi = new ArrayList<ItemMeLi>();
        }

        @Override
        protected void onPostExecute(ApiResponse apiResponse) {
            findViewById(R.id.pg_loading).setVisibility(View.GONE);
            if (apiResponse == null) {
                toastMake("No answer");
            } else {
                populateListViewApiResponse(apiResponse.getContent());
            }
        }

        @Override
        protected ApiResponse doInBackground(Command... params) {
            return params[0].executeCommand();
        }
    }


    private abstract class Command {
        abstract ApiResponse executeCommand();
    }


    private void toastMake(String message){
        Toast toast = Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void deleteAppData() {
        try {
            // clearing app data
            String packageName = getApplicationContext().getPackageName();
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear "+packageName);

        } catch (Exception e) {
            Log.e("deleteAppDataError", e.getMessage());
        }
    }

    private void populateListViewApiResponse(String apiResponseContent){
        try {
            apiRes = apiResponseContent;
            JSONObject jsonResponse = new JSONObject(apiResponseContent);
            jsonItems = jsonResponse.getJSONArray("results");
            for(int i = 0; i < jsonItems.length(); i++){
                JSONObject json = jsonItems.getJSONObject(i);
                Log.d("titles", json.getString("title"));
                ItemMeLi itmMeli = new ItemMeLi();
                itmMeli.setId(json.getString("id"));
                itmMeli.setTitle(json.getString("title"));
                itmMeli.setPrice(json.getInt("price"));
                itmMeli.setThumbnail(json.getString("thumbnail"));

                itemsMeLi.add(itmMeli);
            }

            lvAdapter = new ListViewAdapter(activity, itemsMeLi);
            lvItems.setAdapter(lvAdapter);
            lvItems.setVisibility(View.VISIBLE);

        }catch (Exception e){
            Log.e("error", e.getMessage());
            toastMake("error :(, enter again pls");
            deleteAppData();

        }
    }
}

