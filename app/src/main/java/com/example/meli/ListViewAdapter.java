package com.example.meli;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;




public class ListViewAdapter extends BaseAdapter {

    Activity activity;
    ArrayList<ItemMeLi> myArrayList = new ArrayList<ItemMeLi>();
    LayoutInflater layoutInflater = null;

    public ListViewAdapter(Activity activity, ArrayList myArrayList){
        this.activity = activity;
        this.myArrayList = myArrayList;
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return myArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return getItemId(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private static class ViewHolder{
        ImageView ivThumbnail;
        TextView tv_title, tv_price;

    }

    ViewHolder viewHolder = null;

    // this method  is called each time for arraylist data size.
    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        View v = view;
        final int position = pos;
        if(v == null){
            // create  viewholder object for list_rowcell View.
            viewHolder = new ViewHolder();

            // inflate list_item for each row
            v = layoutInflater.inflate(R.layout.list_item,null);
            viewHolder.ivThumbnail = (ImageView) v.findViewById(R.id.iv_itm_image);
            viewHolder.tv_title = (TextView) v.findViewById(R.id.tv_itm_title);
            viewHolder.tv_price = (TextView) v.findViewById(R.id.tv_itm_price);

            /*We use setTag() and getTag() to set and get custom objects as per our requirement.
            The setTag() method takes an argument of type Object, and getTag() returns an Object.*/
            v.setTag(viewHolder);

        }else{
            /* We recycle a View that already exists */
            viewHolder = (ViewHolder) v.getTag();
        }

        Picasso.get().load(myArrayList.get(position).getThumbnail()).into(viewHolder.ivThumbnail);

        viewHolder.tv_title.setText(myArrayList.get(position).getTitle());
        viewHolder.tv_price.setText("$ "+myArrayList.get(position).getPrice());

        return v;
    }
}
