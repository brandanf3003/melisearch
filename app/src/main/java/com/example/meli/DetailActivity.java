package com.example.meli;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mercadolibre.android.sdk.ApiResponse;
import com.mercadolibre.android.sdk.Meli;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    ImageView ivDetail;
    TextView tvTitle;
    TextView tvDetail;
    TextView tvPrice;
    TextView tvMercadoPago;
    TextView tvStatus;
    TextView tvPermaLink;
    String itmJsonString;
    JSONObject itemJson;
    String detailsItmID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent i = getIntent();
        itmJsonString = i.getStringExtra("itemJson");

        try {
            itemJson = new JSONObject(itmJsonString);
            detailsItmID = itemJson.getString("id");
            inicializeUI(itemJson);
        } catch (JSONException e) {
            Log.e("Detail ERROR", e.getMessage());
        }



    }

    private void inicializeUI(JSONObject json){
        ivDetail = (ImageView) findViewById(R.id.iv_detail);
        tvTitle = (TextView) findViewById(R.id.tv_detail_title);
        tvDetail = (TextView) findViewById(R.id.tv_detail_text);
        tvPrice = (TextView) findViewById(R.id.tv_detail_price);
        tvMercadoPago = (TextView) findViewById(R.id.tv_detail_mercadopago);
        tvStatus = (TextView) findViewById(R.id.tv_detail_status);
        tvPermaLink = (TextView) findViewById(R.id.tv_detail_permalink);



        try {
            Picasso.get().load(json.getString("thumbnail")).into(ivDetail);
            tvTitle.setText(json.getString("title"));
            tvDetail.setText(json.getString("title"));
            tvDetail.setVisibility(View.GONE);
            tvPrice.setText("$" + json.getInt("price"));
            if(json.getString("condition").equals("new")){
                tvStatus.setText("Estado: NUEVO");
            }else{
                tvStatus.setText("Estado: USADO");
            }
            if(json.getBoolean("accepts_mercadopago") == true){
                tvMercadoPago.setText("Acepta Mercadopago: SI");
            }else{
                tvMercadoPago.setText("Acepta Mercadopago: NO");
            }
            tvPermaLink.setText("Link Directo: "+json.getString("permalink"));
            inicializeClickListener(json.getString("permalink"));

            new GetAsycTask().execute(new Command() {
                @Override
                ApiResponse executeCommand() {
                    return Meli.get("/items/"+detailsItmID+"/description");
                }
            });
        } catch (JSONException e) {
            Log.e("Detail UI ERROR", e.getMessage());
        }
    }

    private void inicializeClickListener(final String link){
        tvPermaLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(link));
                startActivity(i);
            }
        });
    }

    private class GetAsycTask extends AsyncTask<DetailActivity.Command, Void, ApiResponse> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(ApiResponse apiResponse) {
            if (apiResponse == null) {
                toastMake("No answer");
            } else {
                Log.d("item", apiResponse.getContent());
                try {
                    JSONObject jsonItemDetail = new JSONObject(apiResponse.getContent());
                    tvDetail.setText("Detalles:\n"+jsonItemDetail.getString("plain_text"));
                    tvDetail.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    Log.e("DETAIL ERROR", e.getMessage());
                }
            }
        }

        @Override
        protected ApiResponse doInBackground(DetailActivity.Command... params) {
            return params[0].executeCommand();
        }
    }


    private abstract class Command {
        abstract ApiResponse executeCommand();
    }

    private void toastMake(String message){
        Toast toast = Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }
}
